﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Blog.Helpers;
using Blog.Models;

namespace Blog.Controllers
{
    public class PostsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Posts
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult List()
        {
            return View(db.Posts.ToList());
        }

        // GET: Posts
        public ActionResult Index()
        {
            var post = db.Posts.Where(p => p.PostedOn.HasValue).OrderByDescending(p => p.PostedOn).ToList();
            return View(post);
        }

        public ActionResult Full(int id)
        {
            var post = db.Posts.Include(p => p.Comments).Where(p => p.Id == id).FirstOrDefault();
            return View("FullPost", post);
        }
        // GET: Posts/Details/5
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // GET: Posts/Create
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult Create()
        {
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title");
            return View();
        }

        // POST: Posts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult Create([Bind(Include = "Id,Title,Content,CreatedOn,UpdatedOn,PostedOn,UserFullName")] Post post)
        {
            if (ModelState.IsValid)
            {
                post.CreatedOn = DateTime.Now;
                post.UpdatedOn = DateTime.Now;
                post.PostedOn = DateTime.Now;
                post.UserFullName = UserHelper.GetUserName(db.Users, User.Identity);
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("List");
            }

            return View(post);
        }

        // GET: Posts/Edit/5
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult Edit([Bind(Include = "Id,Title,Content,CreatedOn,UpdatedOn,PostedOn,UserFullName")] Post post)
        {
            if (ModelState.IsValid)
            {
                post.UpdatedOn = DateTime.Now;
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(post);
        }

        // GET: Posts/Delete/5
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("List");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
