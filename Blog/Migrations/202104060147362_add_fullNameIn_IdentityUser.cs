namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_fullNameIn_IdentityUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "FullName", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Posts", "Content", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Posts", "Content", c => c.String());
            DropColumn("dbo.AspNetUsers", "FullName");
        }
    }
}
