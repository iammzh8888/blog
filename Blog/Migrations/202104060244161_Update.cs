namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Comments", "UserFullName", c => c.String(maxLength: 255));
            AlterColumn("dbo.Posts", "PostedOn", c => c.DateTime());
            AlterColumn("dbo.Posts", "UserFullName", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Posts", "UserFullName", c => c.String());
            AlterColumn("dbo.Posts", "PostedOn", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Comments", "UserFullName", c => c.String());
        }
    }
}
